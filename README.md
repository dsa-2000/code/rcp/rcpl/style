# Style files for rcpl

`.clang-format` and `.clang-tidy` for `rcpl` sub-groups and projects.

Add this project as a submodule to any project within the `rcpl`
group, and create symlinks in the project top level directory to the
files in the submodule.
